What's this?
------------

An OpenTTD NewGRF for town names based on names from the Fediverse.

How do I use it?
----------------

You can compile it with nmlc to get `feditownnames.grf`, which you can then put in OpenTTD's newgrf directory.